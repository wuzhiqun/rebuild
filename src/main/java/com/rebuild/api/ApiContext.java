/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.api;

import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.privileges.UserService;
import com.rebuild.utils.JSONUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import java.util.Collections;
import java.util.Map;


public class ApiContext {

    final private String appId;
    final private ID bindUser;
    final private Map<String, String> reqParams;
    final private JSON postData;

    
    public ApiContext(Map<String, String> reqParams) {
        this(reqParams, null, null, UserService.SYSTEM_USER);
    }

    
    public ApiContext(Map<String, String> reqParams, JSON postData) {
        this(reqParams, postData, null, UserService.SYSTEM_USER);
    }

    
    public ApiContext(Map<String, String> reqParams, JSON postData, String appId, ID bindUser) {
        this.reqParams = Collections.unmodifiableMap(reqParams);
        this.postData = postData;
        this.appId = appId;
        this.bindUser = bindUser;
    }

    
    public String getAppId() {
        return appId;
    }

    
    public ID getBindUser() {
        if (bindUser == null) return UserService.SYSTEM_USER;
        else return bindUser;
    }

    
    public Map<String, String> getParameterMap() {
        return reqParams;
    }

    
    public JSON getPostData() {
        return postData == null ? new JSONObject() : JSONUtils.clone(postData);
    }

    
    public String getParameterNotBlank(String name) throws ApiInvokeException {
        String value = reqParams.get(name);
        if (StringUtils.isBlank(value)) {
            throw new ApiInvokeException(ApiInvokeException.ERR_BADPARAMS, "Parameter [" + name + "] cannot be null");
        }
        return value;
    }

    
    public String getParameter(String name) {
        return reqParams.get(name);
    }

    
    public ID getParameterAsId(String name) {
        String value = getParameterNotBlank(name);
        if (ID.isId(value)) return ID.valueOf(value);
        throw new ApiInvokeException(ApiInvokeException.ERR_BADPARAMS, "Parameter [" + name + "] is invalid");
    }

    
    public int getParameterAsInt(String name, int defaultValue) {
        String value = reqParams.get(name);
        if (NumberUtils.isNumber(value)) return NumberUtils.toInt(value);
        else return defaultValue;
    }

    
    public boolean getParameterAsBool(String name, boolean defaultValue) {
        String value = reqParams.get(name);
        if (StringUtils.isBlank(value)) return defaultValue;
        else return BooleanUtils.toBoolean(value);
    }
}
