/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.task;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSON;
import com.rebuild.core.UserContextHolder;
import com.rebuild.core.support.SetUser;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;


@Slf4j
public abstract class HeavyTask<T> extends SetUser implements Runnable {

    volatile private boolean interruptState = false;

    
    private ID threadUser;

    
    private int total = -1;
    
    private int completed = 0;
    
    private int succeeded = 0;

    final private Date beginTime;
    private Date completedTime;

    protected String errorMessage;

    protected HeavyTask() {
        this.beginTime = CalendarUtils.now();
    }

    @Override
    public SetUser setUser(ID user) {
        this.threadUser = user;
        return super.setUser(user);
    }

    protected void setTotal(int total) {
        this.total = total;
    }

    protected void setCompleted(int completed) {
        this.completed = completed;
    }

    protected void addCompleted() {
        this.completed++;
    }

    protected Date getCompletedTime() {
        return completedTime;
    }

    protected void addSucceeded() {
        succeeded++;
    }

    
    public long getElapsedTime() {
        if (getCompletedTime() != null) {
            return getCompletedTime().getTime() - beginTime.getTime();
        } else {
            return CalendarUtils.now().getTime() - beginTime.getTime();
        }
    }

    
    public int getTotal() {
        return total;
    }

    
    public int getCompleted() {
        return completed;
    }

    
    public double getCompletedPercent() {
        if (total == -1 || completed == 0) {
            return 0;
        }
        if (completed >= total) {
            return 1;
        }
        return completed * 1d / total;
    }

    
    public boolean isCompleted() {
        return getCompletedTime() != null || (total != -1 && getCompleted() >= getTotal());
    }

    
    public int getSucceeded() {
        return succeeded;
    }

    
    public String getErrorMessage() {
        return errorMessage;
    }

    
    public JSON getExecResults() {
        return null;
    }

    

    
    public void setInterruptState() {
        this.interruptState = true;
    }

    
    public boolean isInterruptState() {
        if (this.interruptState) return true;
        if (Thread.currentThread().isInterrupted()) {
            log.warn("Current thread is interrupted (by VM) : {}", Thread.currentThread());
            setInterruptState();
        }
        return interruptState;
    }

    @Override
    final public void run() {
        if (this.threadUser != null) {
            UserContextHolder.setUser(this.threadUser);
        }

        try {
            exec();
        } catch (Exception ex) {
            log.error("Exception during task execute", ex);
            this.errorMessage = ex.getLocalizedMessage();
        } finally {
            completedAfter();
        }
    }

    
    abstract protected T exec() throws Exception;

    
    protected void completedAfter() {
        this.completedTime = CalendarUtils.now();
        if (this.threadUser != null) {
            UserContextHolder.clearUser();
        }
    }

    @Override
    public String toString() {
        return "HeavyTask#" + getClass();
    }
}
