/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.query;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.commons.ObjectUtils;
import cn.devezhao.momentjava.Moment;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.dialect.FieldType;
import cn.devezhao.persist4j.dialect.Type;
import cn.devezhao.persist4j.engine.ID;
import cn.devezhao.persist4j.query.compiler.QueryCompiler;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.UserContextHolder;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.easymeta.DisplayType;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.metadata.impl.EasyFieldConfigProps;
import com.rebuild.core.privileges.UserHelper;
import com.rebuild.core.privileges.bizz.Department;
import com.rebuild.core.support.License;
import com.rebuild.core.support.SetUser;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.CommonsUtils;
import com.rebuild.utils.JSONUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static cn.devezhao.commons.CalendarUtils.addDay;
import static cn.devezhao.commons.CalendarUtils.addMonth;


@Slf4j
public class AdvFilterParser extends SetUser {

    
    public static final String VF_ACU = "$APPROVALCURRENTUSER$";

    
    private static final String MODE_QUICK = "QUICK";

    
    private static final String NAME_FIELD_PREFIX = "" + QueryCompiler.NAME_FIELD_PREFIX;

    final private JSONObject filterExpr;
    final private Entity rootEntity;
    
    final private ID varRecord;

    transient private Set<String> includeFields = null;

    
    public AdvFilterParser(JSONObject filterExpr) {
        this(filterExpr, MetadataHelper.getEntity(filterExpr.getString("entity")));
    }

    
    public AdvFilterParser(JSONObject filterExpr, Entity rootEntity) {
        Assert.notNull(filterExpr, "[filterExpr] cannot be null");
        this.filterExpr = filterExpr;
        this.rootEntity = rootEntity;
        this.varRecord = null;

        String entityName = filterExpr.getString("entity");
        if (entityName != null) {
            Assert.isTrue(entityName.equalsIgnoreCase(this.rootEntity.getName()),
                    "Filter#2 uses different entities : " + entityName + "/" + this.rootEntity.getName());
        }
    }

    
    public AdvFilterParser(JSONObject filterExpr, ID varRecord) {
        this.filterExpr = filterExpr;
        this.rootEntity = MetadataHelper.getEntity(varRecord.getEntityCode());
        this.varRecord = License.isRbvAttached() ? varRecord : null;

        String entityName = filterExpr.getString("entity");
        if (entityName != null) {
            Assert.isTrue(entityName.equalsIgnoreCase(this.rootEntity.getName()),
                    "Filter#3 uses different entities : " + entityName + "/" + this.rootEntity.getName());
        }
    }

    
    public String toSqlWhere() {
        if (filterExpr == null || filterExpr.isEmpty()) return null;

        this.includeFields = new HashSet<>();

        
        if (MODE_QUICK.equalsIgnoreCase(filterExpr.getString("type"))) {
            String quickFields = filterExpr.getString("quickFields");
            JSONArray quickItems = buildQuickFilterItems(quickFields, 1);

















            filterExpr.put("items", quickItems);
        }

        JSONArray items = filterExpr.getJSONArray("items");
        items = items == null ? JSONUtils.EMPTY_ARRAY : items;

        JSONObject values = filterExpr.getJSONObject("values");
        values = values == null ? JSONUtils.EMPTY_OBJECT : values;

        String equation = StringUtils.defaultIfBlank(filterExpr.getString("equation"), "OR");

        Map<Integer, String> indexItemSqls = new LinkedHashMap<>();
        int incrIndex = 1;
        for (Object o : items) {
            JSONObject item = (JSONObject) o;
            Integer index = item.getInteger("index");
            if (index == null) {
                index = incrIndex++;
            }

            String itemSql = parseItem(item, values, rootEntity);
            if (itemSql != null) {
                indexItemSqls.put(index, itemSql.trim());
                this.includeFields.add(item.getString("field"));
            }
            if (CommonsUtils.DEVLOG) System.out.println("[dev] Parse item : " + item + " >> " + itemSql);
        }

        if (indexItemSqls.isEmpty()) return null;

        String equationHold = equation;
        if ((equation = validEquation(equation)) == null) {
            throw new FilterParseException(Language.L("无效的高级表达式 : %s", equationHold));
        }

        if ("OR".equalsIgnoreCase(equation)) {
            return "( " + StringUtils.join(indexItemSqls.values(), " or ") + " )";
        } else if ("AND".equalsIgnoreCase(equation)) {
            return "( " + StringUtils.join(indexItemSqls.values(), " and ") + " )";
        } else {
            
            String[] tokens = equation.toLowerCase().split(" ");
            List<String> itemSqls = new ArrayList<>();
            for (String token : tokens) {
                if (StringUtils.isBlank(token)) {
                    continue;
                }

                boolean hasRP = false;  
                if (token.length() > 1) {
                    if (token.startsWith("(")) {
                        itemSqls.add("(");
                        token = token.substring(1);
                    } else if (token.endsWith(")")) {
                        hasRP = true;
                        token = token.substring(0, token.length() - 1);
                    }
                }

                if (NumberUtils.isDigits(token)) {
                    String itemSql = StringUtils.defaultIfBlank(indexItemSqls.get(Integer.valueOf(token)), "(9=9)");
                    itemSqls.add(itemSql);
                } else if ("(".equals(token) || ")".equals(token) || "or".equals(token) || "and".equals(token)) {
                    itemSqls.add(token);
                } else {
                    log.warn("Invalid equation token : " + token);
                }

                if (hasRP) {
                    itemSqls.add(")");
                }
            }
            return "( " + StringUtils.join(itemSqls, " ") + " )";
        }
    }

    
    public Set<String> getIncludeFields() {
        Assert.notNull(includeFields, "Calls #toSqlWhere first");
        return includeFields;
    }

    
    private String parseItem(JSONObject item, JSONObject values, Entity specRootEntity) {
        String field = item.getString("field");
        if (field.startsWith("&amp;")) field = field.replace("&amp;", NAME_FIELD_PREFIX);  

        final boolean hasNameFlag = field.startsWith(NAME_FIELD_PREFIX);
        if (hasNameFlag) field = field.substring(1);

        Field lastFieldMeta = VF_ACU.equals(field)
                ? specRootEntity.getField(EntityHelper.ApprovalLastUser)
                : MetadataHelper.getLastJoinField(specRootEntity, field);
        if (lastFieldMeta == null) {
            log.warn("Invalid field : {} in {}", field, specRootEntity.getName());
            return null;
        }

        DisplayType dt = EasyMetaFactory.getDisplayType(lastFieldMeta);

        if (dt == DisplayType.CLASSIFICATION || (dt == DisplayType.PICKLIST && hasNameFlag) ) {
            field = NAME_FIELD_PREFIX + field;
        } else if (hasNameFlag) {
            if (!(dt == DisplayType.REFERENCE || dt == DisplayType.N2NREFERENCE)) {
                log.warn("Non reference-field : {} in {}", field, specRootEntity.getName());
                return null;
            }

            
            if (dt == DisplayType.REFERENCE) {
                lastFieldMeta = lastFieldMeta.getReferenceEntity().getNameField();
                dt = EasyMetaFactory.getDisplayType(lastFieldMeta);
                field += "." + lastFieldMeta.getName();
            }
        }

        String op = item.getString("op");
        String value = useValueOfVarRecord(item.getString("value"), lastFieldMeta);
        String valueEnd = null;

        if (dt == DisplayType.N2NREFERENCE) {
            String inWhere = null;
            boolean forceNot = false;
            if (hasNameFlag) {
                Entity refEntity = lastFieldMeta.getReferenceEntity();
                Field nameField = refEntity.getNameField();

                JSONObject fakeItem = (JSONObject) JSONUtils.clone(item);
                fakeItem.put("field", nameField.getName());
                fakeItem.put("op", fakeItem.getString("op"));

                
                String opCheck = fakeItem.getString("op");
                forceNot = ParseHelper.NLK.equalsIgnoreCase(opCheck) || ParseHelper.NEQ.equalsIgnoreCase(opCheck);
                if (forceNot) fakeItem.put("op", opCheck.substring(1));  

                String realWhereSql = parseItem(fakeItem, null, refEntity);
                inWhere = String.format("select %s from %s where %s",
                        refEntity.getPrimaryField().getName(), refEntity.getName(), realWhereSql);
            }
            
            else if (ParseHelper.IN.equals(op) && ID.isId(value)) {
                inWhere = quoteValue(value, FieldType.STRING);
            }

            if (inWhere != null) {
                String xJoinField = specRootEntity.getPrimaryField().getName();
                if (StringUtils.countMatches(field, ".") > 0) {
                    xJoinField = field.substring(0, field.lastIndexOf("."));
                }

                String inWhere2 = String.format(
                        " in (select recordId from NreferenceItem where belongEntity = '%s' and belongField = '%s' and referenceId in (%s))",
                        lastFieldMeta.getOwnEntity().getName(), lastFieldMeta.getName(), inWhere);

                if (forceNot) inWhere2 = " not" + inWhere2;
                return xJoinField + inWhere2;
            }

            

        } else if (dt == DisplayType.TAG && (ParseHelper.IN.equals(op) || ParseHelper.NIN.equals(op))) {
            String xJoinField = specRootEntity.getPrimaryField().getName();
            if (StringUtils.countMatches(field, ".") > 0) {
                xJoinField = field.substring(0, field.lastIndexOf("."));
            }

            String inWhere = String.format(
                    " in (select recordId from TagItem where belongEntity = '%s' and belongField = '%s' and tagName in (%s))",
                    lastFieldMeta.getOwnEntity().getName(), lastFieldMeta.getName(), quoteValue(value, FieldType.STRING));

            if (ParseHelper.NIN.equals(op)) inWhere = " not" + inWhere;
            return xJoinField + inWhere;
        }

        

        final boolean isDatetime = dt == DisplayType.DATETIME;

        
        if (isDatetime || dt == DisplayType.DATE) {

            final boolean isREX = ParseHelper.RED.equalsIgnoreCase(op)
                    || ParseHelper.REM.equalsIgnoreCase(op)
                    || ParseHelper.REY.equalsIgnoreCase(op);

            if (ParseHelper.TDA.equalsIgnoreCase(op)
                    || ParseHelper.YTA.equalsIgnoreCase(op)
                    || ParseHelper.TTA.equalsIgnoreCase(op)
                    || ParseHelper.DDD.equalsIgnoreCase(op)
                    || ParseHelper.EVW.equalsIgnoreCase(op) || ParseHelper.EVM.equalsIgnoreCase(op)) {

                if (ParseHelper.DDD.equalsIgnoreCase(op)) {
                    int x = NumberUtils.toInt(value);
                    value = formatDate(addDay(x), 0);
                } else if (ParseHelper.EVW.equalsIgnoreCase(op) || ParseHelper.EVM.equalsIgnoreCase(op)) {
                    final Calendar today = CalendarUtils.getInstance();

                    int x = NumberUtils.toInt(value);
                    if (ParseHelper.EVW.equalsIgnoreCase(op)) {
                        boolean isSunday = today.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
                        if (isSunday) today.add(Calendar.DAY_OF_WEEK, -1);
                        if (x < 1) x = 1;
                        if (x > 7) x = 7;
                        x += 1;
                        if (x <= 7) {
                            today.set(Calendar.DAY_OF_WEEK, x);
                        } else {
                            today.set(Calendar.DAY_OF_WEEK, 7);
                            today.add(Calendar.DAY_OF_WEEK, 1);
                        }
                    } else {
                        if (x < 1) x = 1;
                        if (x > 31) x = 31;

                        
                        int maxDayOfMonth = today.getActualMaximum(Calendar.DAY_OF_MONTH);
                        if (x > maxDayOfMonth) x = maxDayOfMonth;

                        today.set(Calendar.DAY_OF_MONTH, x);
                    }
                    value = formatDate(today.getTime(), 0);
                }
                else if (ParseHelper.YTA.equalsIgnoreCase(op)) {
                    value = formatDate(addDay(-1), 0);
                } else if (ParseHelper.TTA.equalsIgnoreCase(op)) {
                    value = formatDate(addDay(1), 0);
                } else {
                    value = formatDate(CalendarUtils.now(), 0);
                }

                if (isDatetime) {
                    op = ParseHelper.BW;
                    valueEnd = parseValue(value, op, lastFieldMeta, true);
                }

            } else if (ParseHelper.CUW.equalsIgnoreCase(op)
                    || ParseHelper.CUM.equalsIgnoreCase(op)
                    || ParseHelper.CUQ.equalsIgnoreCase(op)
                    || ParseHelper.CUY.equalsIgnoreCase(op)
                    || ParseHelper.PUW.equalsIgnoreCase(op)
                    || ParseHelper.PUM.equalsIgnoreCase(op)
                    || ParseHelper.PUQ.equalsIgnoreCase(op)
                    || ParseHelper.PUY.equalsIgnoreCase(op)
                    || ParseHelper.NUW.equalsIgnoreCase(op)
                    || ParseHelper.NUM.equalsIgnoreCase(op)
                    || ParseHelper.NUQ.equalsIgnoreCase(op)
                    || ParseHelper.NUY.equalsIgnoreCase(op)) {

                String unit = op.substring(2);
                int amount = op.startsWith("P") ? -1 : (op.startsWith("N") ? 1 : 0);

                Date begin = Moment.moment().startOf(op.substring(2)).add(amount, unit).date();
                value = formatDate(begin, 0);

                Date end = Moment.moment(begin).endOf(unit).date();
                valueEnd = formatDate(end, 0);

                if (isDatetime) {
                    value += ParseHelper.ZERO_TIME;
                    valueEnd += ParseHelper.FULL_TIME;
                }
                op = ParseHelper.BW;

            } else if (ParseHelper.EQ.equalsIgnoreCase(op)
                    && dt == DisplayType.DATETIME && StringUtils.length(value) == 10) {

                op = ParseHelper.BW;
                valueEnd = parseValue(value, op, lastFieldMeta, true);

            } else if (isREX
                    || ParseHelper.FUD.equalsIgnoreCase(op)
                    || ParseHelper.FUM.equalsIgnoreCase(op)
                    || ParseHelper.FUY.equalsIgnoreCase(op)) {

                int xValue = NumberUtils.toInt(value) * (isREX ? -1 : 1);
                Date date;
                if (ParseHelper.REM.equalsIgnoreCase(op) || ParseHelper.FUM.equalsIgnoreCase(op)) {
                    date = CalendarUtils.addMonth(xValue);
                } else if (ParseHelper.REY.equalsIgnoreCase(op) || ParseHelper.FUY.equalsIgnoreCase(op)) {
                    date = CalendarUtils.addMonth(xValue * 12);
                } else {
                    date = CalendarUtils.addDay(xValue);
                }

                if (isREX) {
                    value = formatDate(date, 0);
                    valueEnd = formatDate(CalendarUtils.now(), 0);
                } else {
                    value = formatDate(CalendarUtils.now(), 0);
                    valueEnd = formatDate(date, 0);
                }

                if (isDatetime) {
                    value += ParseHelper.ZERO_TIME;
                    valueEnd += ParseHelper.FULL_TIME;
                }
                op = ParseHelper.BW;

            }

        } else if (dt == DisplayType.TIME) {
            
            if (value != null && value.length() == 5) {
                if (ParseHelper.EQ.equalsIgnoreCase(op)) {
                    op = ParseHelper.BW;
                    valueEnd = value + ":59";
                }
                value += ":00";
            }

        } else if (dt == DisplayType.MULTISELECT) {
            if (ParseHelper.IN.equalsIgnoreCase(op) || ParseHelper.NIN.equalsIgnoreCase(op)
                    || ParseHelper.EQ.equalsIgnoreCase(op) || ParseHelper.NEQ.equalsIgnoreCase(op)) {
                
                if (ParseHelper.IN.equalsIgnoreCase(op)) op = ParseHelper.BAND;
                else if (ParseHelper.NIN.equalsIgnoreCase(op)) op = ParseHelper.NBAND;

                long maskValue = 0;
                for (String s : value.split("\\|")) {
                    maskValue += ObjectUtils.toLong(s);
                }
                value = String.valueOf(maskValue);
            }
        }

        StringBuilder sb = new StringBuilder(field)
                .append(' ')
                .append(ParseHelper.convetOperation(op));
        
        if (ParseHelper.NL.equalsIgnoreCase(op) || ParseHelper.NT.equalsIgnoreCase(op)) {
            return sb.toString();
        }

        sb.append(' ');

        

        if (ParseHelper.BFD.equalsIgnoreCase(op)) {
            value = formatDate(addDay(-NumberUtils.toInt(value)), isDatetime ? 1 : 0);
        } else if (ParseHelper.BFM.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(-NumberUtils.toInt(value)), isDatetime ? 1 : 0);
        } else if (ParseHelper.BFY.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(-NumberUtils.toInt(value) * 12), isDatetime ? 1 : 0);
        } else if (ParseHelper.AFD.equalsIgnoreCase(op)) {
            value = formatDate(addDay(NumberUtils.toInt(value)), isDatetime ? 2 : 0);
        } else if (ParseHelper.AFM.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(NumberUtils.toInt(value)), isDatetime ? 2 : 0);
        } else if (ParseHelper.AFY.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(NumberUtils.toInt(value) * 12), isDatetime ? 2 : 0);
        }
        
        else if (ParseHelper.SFU.equalsIgnoreCase(op)) {
            value = getUser().toLiteral();
        } else if (ParseHelper.SFB.equalsIgnoreCase(op)) {
            Department dept = UserHelper.getDepartment(getUser());
            if (dept != null) {
                value = dept.getIdentity().toString();
                int ref = lastFieldMeta.getReferenceEntity().getEntityCode();
                if (ref == EntityHelper.User) {
                    sb.insert(sb.indexOf(" "), ".deptId");
                } else if (ref == EntityHelper.Department) {
                    
                } else {
                    value = null;
                }
            }
        } else if (ParseHelper.SFD.equalsIgnoreCase(op)) {
            Department dept = UserHelper.getDepartment(getUser());
            if (dept != null) {
                int refe = lastFieldMeta.getReferenceEntity().getEntityCode();
                if (refe == EntityHelper.Department) {
                    value = StringUtils.join(UserHelper.getAllChildren(dept), "|");
                }
            }
        } else if (ParseHelper.SFT.equalsIgnoreCase(op)) {
            if (value == null) value = "0";  
            
            value = String.format(
                    "( select userId from TeamMember where teamId in ('%s') )",
                    StringUtils.join(value.split("\\|"), "', '"));
        } else if (ParseHelper.REP.equalsIgnoreCase(op)) {
            
            value = MessageFormat.format(
                    "( select {0} from {1} group by {0} having (count({0}) > {2}) )",
                    field, rootEntity.getName(), NumberUtils.toInt(value, 1));
        }

        if (StringUtils.isBlank(value)) {
            log.warn("No search value defined : " + item.toJSONString());
            return null;
        }

        
        if (value.matches("\\{\\d+}")) {
            if (values == null || values.isEmpty()) return null;

            String valHold = value.replaceAll("[{}]", "");
            value = parseValue(values.get(valHold), op, lastFieldMeta, false);
        } else {
            value = parseValue(value, op, lastFieldMeta, false);
        }

        
        if (value == null) return null;

        
        final boolean isBetween = op.equalsIgnoreCase(ParseHelper.BW);
        if (isBetween && valueEnd == null) {
            valueEnd = useValueOfVarRecord(item.getString("value2"), lastFieldMeta);
            valueEnd = parseValue(valueEnd, op, lastFieldMeta, true);
            if (valueEnd == null) valueEnd = value;
        }

        
        if (op.equalsIgnoreCase(ParseHelper.IN) || op.equalsIgnoreCase(ParseHelper.NIN)
                || op.equalsIgnoreCase(ParseHelper.SFD) || op.equalsIgnoreCase(ParseHelper.SFT)
                || op.equalsIgnoreCase(ParseHelper.REP)) {
            sb.append(value);
        } else {
            
            if (op.equalsIgnoreCase(ParseHelper.LK) || op.equalsIgnoreCase(ParseHelper.NLK)) {
                value = '%' + value + '%';
            }
            sb.append(quoteValue(value, lastFieldMeta.getType()));
        }

        if (isBetween) {
            sb.insert(0, "( ")
                    .append(" and ").append(quoteValue(valueEnd, lastFieldMeta.getType()))
                    .append(" )");
        }

        if (VF_ACU.equals(field)) {
            return String.format(
                    "(exists (select recordId from RobotApprovalStep where ^%s = recordId and state = 1 and isCanceled = 'F' and %s) and approvalState = 2)",
                    specRootEntity.getPrimaryField().getName(), sb.toString().replace(VF_ACU, "approver"));
        } else {
            return sb.toString();
        }
    }

    
    private String parseValue(Object val, String op, Field field, boolean fullTime) {
        String value;
        
        if (val instanceof JSONArray) {
            Set<String> inVals = new HashSet<>();
            for (Object v : (JSONArray) val) {
                inVals.add(quoteValue(v.toString(), field.getType()));
            }
            return optimizeIn(inVals);

        } else {
            value = val == null ? null : val.toString();
            if (StringUtils.isBlank(value)) return null;

            final int valueLen = StringUtils.length(value);

            
            if (field.getType() == FieldType.TIMESTAMP && valueLen == 10) {
                if (ParseHelper.GT.equalsIgnoreCase(op)) {
                    value += ParseHelper.FULL_TIME;  
                } else if (ParseHelper.LT.equalsIgnoreCase(op)) {
                    value += ParseHelper.ZERO_TIME;  
                } else if (ParseHelper.GE.equalsIgnoreCase(op)) {
                    value += ParseHelper.ZERO_TIME;  
                } else if (ParseHelper.LE.equalsIgnoreCase(op)) {
                    value += ParseHelper.FULL_TIME;  
                } else if (ParseHelper.BW.equalsIgnoreCase(op)) {
                    value += (fullTime ? ParseHelper.FULL_TIME : ParseHelper.ZERO_TIME);  
                }
            }
            
            else if (field.getType() == FieldType.DATE && valueLen == 10) {
                String dateFormat = StringUtils.defaultIfBlank(
                        EasyMetaFactory.valueOf(field).getExtraAttr(EasyFieldConfigProps.DATE_FORMAT),
                        DisplayType.DATE.getDefaultFormat());

                if (dateFormat.length() == 4) {
                    value = value.substring(0, 4) + "-01-01";
                } else if (dateFormat.length() == 7) {
                    value = value.substring(0, 7) + "-01";
                }
            }

            
            if (op.equalsIgnoreCase(ParseHelper.IN) || op.equalsIgnoreCase(ParseHelper.NIN)
                    || op.equalsIgnoreCase(ParseHelper.SFD)) {
                Set<String> inVals = new HashSet<>();
                for (String v : value.split("\\|")) {
                    inVals.add(quoteValue(v, field.getType()));
                }
                return optimizeIn(inVals);
            }
        }
        return value;
    }

    
    private String quoteValue(String val, Type type) {
        if (NumberUtils.isNumber(val) && isNumberType(type)) {
            return val;
        } else if (StringUtils.isNotBlank(val)) {
            return String.format("'%s'", CommonsUtils.escapeSql(val));
        }
        return "''";
    }

    
    private String optimizeIn(Set<String> inVals) {
        if (inVals == null || inVals.isEmpty()) return null;
        else return "( " + StringUtils.join(inVals, ",") + " )";
    }

    
    private boolean isNumberType(Type type) {
        return type == FieldType.INT || type == FieldType.SMALL_INT || type == FieldType.LONG
                || type == FieldType.DOUBLE || type == FieldType.DECIMAL;
    }

    
    private String formatDate(Date date, int paddingType) {
        String s = CalendarUtils.getUTCDateFormat().format(date);
        if (paddingType == 1) s += ParseHelper.FULL_TIME;
        else if (paddingType == 2) s += ParseHelper.ZERO_TIME;
        return s;
    }

    
    private JSONArray buildQuickFilterItems(String quickFields, int valueIndex) {
        Set<String> usesFields = ParseHelper.buildQuickFields(rootEntity, quickFields);

        JSONArray items = new JSONArray();
        for (String field : usesFields) {
            items.add(JSON.parseObject("{ op:'LK', value:'{" + valueIndex + "}', field:'" + field + "' }"));
        }
        return items;
    }

    
    private static final String PATT_FIELDVAR = "\\{@([\\w.]+)}";
    
    private static final String CURRENT_ANY = "CURRENT";
    private static final String CURRENT_DATE = "NOW";

    
    private String useValueOfVarRecord(String value, Field queryField) {
        if (StringUtils.isBlank(value) || !value.matches(PATT_FIELDVAR)) return value;

        
        final String fieldName = value.substring(2, value.length() - 1);

        Object useValue = null;

        
        if (CURRENT_ANY.equals(fieldName) || CURRENT_DATE.equals(fieldName)) {
            DisplayType dt = EasyMetaFactory.getDisplayType(queryField);
            if (dt == DisplayType.DATE || dt == DisplayType.DATETIME || dt == DisplayType.TIME) {
                useValue = CalendarUtils.now();

            } else if (dt == DisplayType.REFERENCE) {
                if (queryField.getReferenceEntity().getEntityCode() == EntityHelper.User) {
                    useValue = UserContextHolder.getUser();
                } else if (queryField.getReferenceEntity().getEntityCode() == EntityHelper.Department) {
                    Department dept = UserHelper.getDepartment(UserContextHolder.getUser());
                    if (dept != null) useValue = dept.getIdentity();
                }

            } else {
                log.warn("Cannot use `CURRENT` in `{}` (None date fields)", queryField);
                return StringUtils.EMPTY;
            }
        }
        
        if (fieldName.startsWith(CURRENT_ANY + ".")) {
            String userField = fieldName.substring(CURRENT_ANY.length() + 1);
            Object[] o = Application.getQueryFactory().uniqueNoFilter(getUser(), userField);
            if (o == null || o[0] == null) return StringUtils.EMPTY;
            else useValue = o[0];
        }

        if (useValue == null) {
            if (varRecord == null) return value;

            Field valueField = MetadataHelper.getLastJoinField(rootEntity, fieldName);
            if (valueField == null) {
                log.warn("Invalid var-field : {} in {}", value, rootEntity.getName());
                return StringUtils.EMPTY;
            }

            Object[] o = Application.getQueryFactory().uniqueNoFilter(varRecord, fieldName);
            if (o == null || o[0] == null) return StringUtils.EMPTY;
            else useValue = o[0];
        }

        if (useValue instanceof Date) {
            useValue = CalendarUtils.getUTCDateFormat().format(useValue);
        } else if (useValue instanceof TemporalAccessor) {
            useValue = CalendarUtils.getDateFormat("HH:mm").format(CalendarUtils.now());
        } else if (useValue instanceof BigDecimal) {
            useValue = String.valueOf(((BigDecimal) useValue).doubleValue());
        } else {
            useValue = String.valueOf(useValue);
        }
        return (String) useValue;
    }

    
    public static String validEquation(String equation) {
        if (StringUtils.isBlank(equation)) {
            return "OR";
        }
        if ("OR".equalsIgnoreCase(equation) || "AND".equalsIgnoreCase(equation)) {
            return equation;
        }

        String clearEquation = equation.toUpperCase()
                .replace("OR", " OR ")
                .replace("AND", " AND ")
                .replaceAll("\\s+", " ")
                .trim();
        equation = clearEquation;

        if (clearEquation.startsWith("AND") || clearEquation.startsWith("OR")
                || clearEquation.endsWith("AND") || clearEquation.endsWith("OR")) {
            return null;
        }
        if (clearEquation.contains("()") || clearEquation.contains("( )")) {
            return null;
        }

        for (String token : clearEquation.split(" ")) {
            token = token.replace("(", "");
            token = token.replace(")", "");

            
            if (NumberUtils.isNumber(token)) {
                if (NumberUtils.toInt(token) > 10) {
                    return null;
                } else {
                    
                }
            } else if ("AND".equals(token) || "OR".equals(token) || "(".equals(token) || ")".equals(token)) {
                
            } else {
                return null;
            }
        }

        
        
        clearEquation = clearEquation.replaceAll("[AND|OR|0-9|\\s]", "");
        
        for (int i = 0; i < 20; i++) {
            clearEquation = clearEquation.replace("()", "");
            if (clearEquation.isEmpty()) return equation;
        }
        return null;
    }

    
    public static boolean hasFieldVars(JSONObject filterExpr) {
        for (Object o : filterExpr.getJSONArray("items")) {
            JSONObject item = (JSONObject) o;
            String value = item.getString("value");
            if (value != null && value.matches(PATT_FIELDVAR)) return true;
            String value2 = item.getString("value2");
            if (value2 != null && value2.matches(PATT_FIELDVAR)) return true;
        }
        return false;
    }
}
