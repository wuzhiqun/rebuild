/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general;

import cn.devezhao.bizz.privileges.impl.BizzPermission;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.service.ServiceSpec;
import com.rebuild.core.service.approval.ApprovalState;
import com.rebuild.core.support.task.TaskExecutors;

import java.util.List;


public interface EntityService extends ServiceSpec {

    
    int delete(ID recordId, String[] cascades);

    
    int assign(ID recordId, ID to, String[] cascades);

    
    default int share(ID recordId, ID to, String[] cascades) {
        return share(recordId, to, cascades, BizzPermission.READ.getMask());
    }

    
    int share(ID recordId, ID to, String[] cascades, int rights);

    
    int unshare(ID recordId, ID accessId);

    
    int bulk(BulkContext context);

    
    String bulkAsync(BulkContext context);

    
    List<Record> getAndCheckRepeated(Record checkRecord, int limit);

    
    void approve(ID recordId, ApprovalState state, ID approvalUser);
}
