/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.configuration;


public interface ConfigManager {

    
    String DELETED_ITEM = "$$RB_DELETED$$";

    
    void clean(Object cacheKey);
}
