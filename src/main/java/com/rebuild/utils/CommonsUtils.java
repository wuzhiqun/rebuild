/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.utils;

import cn.devezhao.commons.ObjectUtils;
import cn.devezhao.commons.ReflectUtils;
import cn.devezhao.persist4j.engine.NullValue;
import com.rebuild.core.Application;
import com.rebuild.core.BootApplication;
import com.rebuild.core.RebuildException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;


@Slf4j
public class CommonsUtils {

    
    public static final boolean DEVLOG = BootApplication.devMode();

    
    public static final String COMM_SPLITER = "$$$$";
    
    public static final String COMM_SPLITER_RE = "\\$\\$\\$\\$";

    private static final Pattern PATT_PLAINTEXT = Pattern.compile("[A-Za-z0-9_\\-\\u4e00-\\u9fa5]+");

    private static final char[] SPECIAL_CHARS = "`~!@#$%^&*()_+=-{}|[];':\",./<>?".toCharArray();

    
    public static boolean isPlainText(String text) {
        return !text.contains(" ") && PATT_PLAINTEXT.matcher(text).matches();
    }

    
    public static boolean isSpecialChar(char ch) {
        for (char c : SPECIAL_CHARS) {
            if (c == ch) {
                return true;
            }
        }
        return false;
    }

    
    public static String maxstr(String text, int maxLength) {
        if (text == null) return null;
        if (text.length() > maxLength) return text.substring(0, maxLength);
        return text;
    }

    
    public static String escapeHtml(Object text) {
        if (text == null || StringUtils.isBlank(text.toString())) {
            return StringUtils.EMPTY;
        }

        
        return text.toString()
                .replace("\"", "&quot;")
                .replace("'", "&apos;")
                .replace(">", "&gt;")
                .replace("<", "&lt;");
    }

    
    public static String sanitizeHtml(Object text) {
        if (text == null || StringUtils.isBlank(text.toString())) {
            return StringUtils.EMPTY;
        }

        
        return text.toString()
                .replace("<script", "")
                .replace("</script>", "")
                .replace("<style", "")
                .replace("</style>", "")
                .replace("<img", "");
    }

    
    public static String escapeSql(Object text) {
        
        text = text.toString().replace("\\'", "'");
        return StringEscapeUtils.escapeSql((String) text);
    }

    
    public static InputStream getStreamOfRes(String file) throws IOException {
        return new ClassPathResource(file).getInputStream();
    }

    
    public static String getStringOfRes(String file) {
        try (InputStream is = getStreamOfRes(file)) {
            return IOUtils.toString(is, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            log.error("Cannot load file of res : " + file);
            return null;
        }
    }

    
    public static String randomHex() {
        return randomHex(false);
    }

    
    public static String randomHex(boolean simple) {
        String hex = UUID.randomUUID().toString();
        if (simple) hex = hex.replace("-", "");
        return hex;
    }

    
    public static int randomInt(int s, int e) {
        int rnd = RandomUtils.nextInt(e);
        return rnd < s ? rnd + s : rnd;
    }

    
    public static Long toLongHalfUp(Object number) {
        double doubleValue = ObjectUtils.toDouble(number);
        return BigDecimal.valueOf(doubleValue)
                .setScale(0, RoundingMode.HALF_UP).longValue();
    }

    
    public static void printStackTrace() {
        if (Application.devMode() || log.isDebugEnabled()) {
            StackTraceElement[] trace = Thread.currentThread().getStackTrace();
            for (StackTraceElement traceElement : trace) {
                System.err.println("\tat " + traceElement);
            }
        }
    }

    
    public static boolean containsIgnoreCase(String[] strs, String search) {
        for (String s : strs) {
            if (StringUtils.containsIgnoreCase(s, search)) return true;
        }
        return false;
    }

    
    public static Object invokeMethod(String desc, Object... args) {
        String[] classAndMethod = desc.split("#");
        try {
            Class<?> clazz = ReflectUtils.classForName(classAndMethod[0]);

            Class<?>[] paramTypes = new Class<?>[args.length];
            for (int i = 0; i < args.length; i++) {
                paramTypes[i] = args[i].getClass();
            }

            Method method = clazz.getMethod(classAndMethod[1], paramTypes);
            return method.invoke(null, args);

        } catch (ReflectiveOperationException ex) {
            log.error("Invalid method invoke : {}", desc);
            throw new RebuildException(ex);
        }
    }

    
    public static boolean isExternalUrl(String str) {
        return str != null
                && (str.startsWith("http://") || str.startsWith("https://"));
    }

    
    public static boolean hasLength(Object any) {
        if (any == null) return false;
        if (any.getClass().isArray()) return ((Object[]) any).length > 0;
        if (any instanceof Collection) return !((Collection<?>) any).isEmpty();
        if (NullValue.is(any)) return false;
        return !any.toString().isEmpty();
    }

    
    @SuppressWarnings("unchecked")
    public static boolean isSame(Object a, Object b) {
        if (a == null && b != null) return false;
        if (a != null && b == null) return false;
        if (Objects.equals(a, b)) return true;

        
        if (a instanceof Number && b instanceof Number) {
            
            return ObjectUtils.toDouble(a) == ObjectUtils.toDouble(b);
        }

        
        if ((a instanceof Collection || a instanceof Object[]) && (b instanceof Collection || b instanceof Object[])) {
            Collection<Object> aColl;
            if (a instanceof Object[]) aColl = Arrays.asList((Object[]) a);
            else aColl = (Collection<Object>) a;
            Collection<Object> bColl;
            if (b instanceof Object[]) bColl = Arrays.asList((Object[]) b);
            else bColl = (Collection<Object>) b;

            if (aColl.size() != bColl.size()) return false;
            if (aColl.isEmpty()) return true;
            return CollectionUtils.containsAll(aColl, bColl) && CollectionUtils.containsAll(bColl, aColl);
        }

        
        
        return StringUtils.equals(a.toString(), b.toString());
    }

    
    public static void checkSafeFilePath(String filepath) throws SecurityException {
        if (filepath == null) return;
        if (filepath.contains("../") || filepath.contains("<") || filepath.contains(">")) {
            throw new SecurityException("Attack path detected : " + escapeHtml(filepath));
        }
    }
}
